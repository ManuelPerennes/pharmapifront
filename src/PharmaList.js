import React, { useState, useEffect } from 'react';
import Pharmacie from './Pharmacie';
import axios from 'axios';

/*
function PharmaList() {

    const [pharmacies, setPharmacies] = useState([]);

    useEffect(() => {
        axios.get('http://127.0.0.1:8080/pharma')
            .then(pharmapi => {
                console.log(pharmapi.data);
                setPharmacies(pharmapi.data);
            });
    }, []);

    return (
        <article>
            <h2>Liste des pharmacies</h2>
            <ul>
                {pharmacies.map(pharmacie => (
                    <li key={pharmacie.id}>
                        {pharmacie.nom}
                    </li>
                ))}
            </ul>
        </article>
    )
}
*/

class PharmaList extends React.Component {

    constructor(props) {
        super(props);
        this.state = { pharmacies: [] };
    }

    componentDidMount() {
        axios.get('http://127.0.0.1:8080/pharma')
            .then(pharmapi => {
                console.log(pharmapi.data);
                this.setState({ pharmacies: pharmapi.data });
            });
    }

    render() {
        return (
            <ul>
                {this.state.pharmacies
                    .filter(p => p.garde === this.props.filter
                        || this.props.filter === '*'
                    )
                    .map(pharmacie => (
                        <Pharmacie key={pharmacie.id} fiche={pharmacie} />
                    ))}
            </ul>
        )
    }
}

export default PharmaList;