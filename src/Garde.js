
function Garde(props){
    return (
        <select name="day" onChange={props.onChange}>
            <option value="*">toutes les pharmacies</option>
            <option value="lundi">lundi</option>
            <option value="mardi">mardi</option>
            <option value="mercredi">mercredi</option>
            <option value="jeudi">jeudi</option>
            <option value="vendredi">vendredi</option>
            <option value="samedi">samedi</option>
            <option value="dimanche">dimanche</option>
        </select>
    );
}

export default Garde;